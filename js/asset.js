$(function () {
                                
    var CatSelect = $('.categories'),
    
    
    myCategories = [ 'RestosETcafés' , 'Santé', 'Sport', 'Food', 'Services' , 'HabillementETmode', 'MaisonETdéco', 'BeautéETsoins', 'Animaux'],
    
    RestosETcafés = [ 'Café/Salon de thé', 'Réstaurant'],

Santé = ['Cabinet Médical', 'Centre d’imagerie' ,'Cliniques', 'Laboratoire d’analyses', 'Opticiens','Pharmacies'],

Sport = ['Club sportifs pour enfants', 'Magasin de compléments alimentaires','Salle de gym','Vente de matériel sportif'],

Food = ['Pâtisseries', 'Traiteurs', 'Vente de gâteaux traditionnels', 'Vente de produits artisanaux'],

Services = ['Agence de communication','Agence de marketing','Agence de voyage', 'Agence immobilière','Boites de développement informatique', 'Bureau d’étude technique','Cabinet d’architecture / Design intérieur', 'Cabinet d’expertise comptable', 'Cabinets d’avocats', 'Imprimeur numérique', 'Maintenance de matériel informatique','Service de location','Service de sécurité', 'Service juridique', 'Service logistique', 'Traducteur','Vente de matériel informatique'],

HabillementETmode = ['Bijouteries','Boutique de robes', 'Magasin d’accessoires', 'Magasin de sacs et valises', 'Magasin de vêtements bébés', 'Magasin de vêtements sportifs', 'Pressing', 'Service retouche de vêtements', 'Vente de lunettes de soleil ','Vente de vêtements pour enfants', 'Vente de vêtements pour femmes', 'Vente de vêtements pour homme'],

MaisonETdéco = ['Magasin d’ameublement','Magasin de design et de décor intérieur','Magasin de literie', 'Magasin de luminaires', 'Magasin de matériel de cuisine', 'Menuiseries  bois et aluminium', 'Pépinière', 'Vente de meubles', 'Vente de mobilier de bureau'],

BeautéETsoins = [' Centre de remise en forme ','Instituts de soin','Magasin de vente cosmétique', 'Parfumerie','Salon de coiffure femmes', 'Salons de coiffure hommes'],


Animaux = ['Animaleries', 'Vente de produits pour animaux',  'Vétérinaires' ];






    // Function Create Option    
    
    function CreateOption(valriable, elementToAppend) {
        
        var i;
        
        for (i = 0; i < valriable.length; i += 1) {
    
            $('<option>', {value: valriable[i], text: valriable[i], id: valriable[i]})
                .appendTo(elementToAppend);
        }
    }
    
    
    CreateOption(myCategories, $('.categories'));
    
    CreateOption(RestosETcafés, $('.RestosETcafés'));
    
    CreateOption(Santé, $('.Santé'));
    
    CreateOption(Sport, $('.Sport'));
    
    CreateOption(Food, $('.Food'));

    CreateOption(Services, $('.Services'));

    CreateOption(HabillementETmode, $('.HabillementETmode'));

    CreateOption(MaisonETdéco, $('.MaisonETdéco'));

    CreateOption(BeautéETsoins, $('.BeautéETsoins'));

    CreateOption(Animaux, $('.Animaux'));
    
    // Hide All Select
    $('.option select').hide();
    
    // Show First Selected
    $('.RestosETcafés').show().css('display', 'inline-block');
    
    
    
    // Show List Option City Whene user Chosse Countries
    
    CatSelect.on('change', function () {
        
        // get Id option 
        var myId = $(this).find(':selected').attr('id');
        console.log($(this).val());

        // Show Select Has class = Id And Hide Siblings
        $('.option select').filter('.' + myId).fadeIn(400).siblings('select').hide();
        
    });
    
});

